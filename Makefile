CC=gcc
FLAGS=-g -std=c11
INCLUDES=$(shell xml2-config --cflags)
LIBS=$(shell xml2-config --libs) $(shell pkg-config --libs libcurl)

Screennews:
	$(CC) $(FLAGS) -c Screennews.c $(INCLUDES) $(LIBS)
all: Screennews
	$(CC) $(FLAGS) -o ./build/screennews main.c Screennews.o $(INCLUDES) -lncurses

clean:
	rm -rf build
