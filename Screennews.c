
#include "Screennews.h"

/** 
 * NewsObj_New
 * Returns an empy NewsObj object allocated in RAM
 * 
 * @return 
 */
NewsObj* NewsObj_New(){
    NewsObj* empty_news=malloc(sizeof(NewsObj));
    memset(empty_news,0,sizeof(NewsObj));

    return empty_news;
}

/** 
 * StoreRSSCallback:
 */
size_t StoreURLCallback( void *contents,
                         size_t size, size_t nmemb,
                         void *userdata){
    size_t realsize = size * nmemb;
    MemoryStruct *mem = (MemoryStruct *)userdata;

    /* increments memory to userdata to store new datas */
    mem->memory = realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL) {
        /* out of memory! */ 
        printf("StoreURLCallback: not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
 
    return realsize;
}

/** 
 * AllocURL:
 */
char* AllocURL(const char* rss_url){
    CURL *curl_handle;
    CURLcode res;
    char* xml;

    xml = NULL;
    MemoryStruct chunk;
 
    chunk.memory = malloc(1);  /* will be grown as needed by the
                                * realloc above */ 
    chunk.size = 0;            /* no data at this point */ 
 
    curl_global_init(CURL_GLOBAL_ALL);
 
    /* init the curl session */ 
    curl_handle = curl_easy_init();
 
    /* specify URL to get */ 
    curl_easy_setopt(curl_handle, CURLOPT_URL, rss_url);
 
    /* send all data to this function  */ 
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, StoreURLCallback);
 
    /* we pass our 'chunk' struct to the callback function */ 
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
 
    /* some servers don't like requests that are made without a user-agent
       field, so we provide one */ 
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
 
    /* get it! */ 
    res = curl_easy_perform(curl_handle);
 
    /* check for errors */ 
    if(res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
    }
    else {
        /*
         * Now, our chunk.memory points to a memory block that is chunk.size
         * bytes big and contains the remote file.
         *
         * Do something nice with it!
         */ 
 
        printf("%lu bytes retrieved\n", (long)chunk.size);
        xml = malloc(chunk.size);
        memcpy(xml,chunk.memory,chunk.size);
    }
 
    /* cleanup curl stuff */ 
    curl_easy_cleanup(curl_handle);
 
    free(chunk.memory);
 
    /* we're done with libcurl, so clean it up */ 
    curl_global_cleanup();
    return xml;
}
/** 
 * LoadItem:
 */
void LoadItem(xmlDoc* doc, xmlNode* item, NewsList* all_news){

    xmlChar* title;
    xmlChar* description;
    xmlChar* link;

    xmlNode* child = item->xmlChildrenNode;
    NewsObj* news = NewsObj_New();

    while (child){
        if (!xmlNodeIsText(child) && xmlStrEqual(child->name,"title") )
            news->title = xmlNodeListGetString(doc,child->xmlChildrenNode,1);

        if (!xmlNodeIsText(child) && xmlStrEqual(child->name,"description") )
            news->descr = xmlNodeListGetString(doc,child->xmlChildrenNode,1);

        if (!xmlNodeIsText(child) && xmlStrEqual(child->name,"guid") )
            news->link = xmlNodeListGetString(doc,child->xmlChildrenNode,1);

        if (!xmlNodeIsText(child) && xmlStrEqual(child->name,"pubDate") )
            news->pubdate = xmlNodeListGetString(doc,child->xmlChildrenNode,1);
        child=child->next;
    }

    all_news->allnews[all_news->total_news] = news;
    all_news->total_news++;
}

/** 
 * ParseRSS
 */
NewsList* ParseRSS(char* xmldocument){
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

        /*parse the file and get the DOM */
        doc = xmlParseDoc(xmldocument);

    if (doc == NULL) {
        printf("error: could not parse XML\n");
        return NULL;
    }

    NewsList* allnews=malloc(sizeof(allnews));
    allnews->total_news=0;

    /* Get the root element node (rss) */
    root_element = xmlDocGetRootElement(doc);
    
    /* get rss childs: looks for "channel" */
    xmlNode* cursor = root_element->xmlChildrenNode;

    if (xmlStrEqual(cursor->name,"channel")==0)
        cursor = cursor->next;

    /* prende il primo figlio di channel */
    cursor = cursor->xmlChildrenNode;

    /* ricerca il primo elemento item */
    while (xmlStrEqual(cursor->name,"item")==0){
        cursor=cursor->next;
    }
    /* finché cursor esiste ed è un item */
    while (cursor){

        if (cursor->type==XML_ELEMENT_NODE &&
            xmlStrEqual(cursor->name,"item")>0){
            LoadItem(doc,cursor,allnews);
        }
        cursor = cursor->next;
    }
       
    /*free the document */
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

    return allnews;
}
