
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define ANSA_TAG "<div itemprop=\"articleBody\" class=\"news-txt\">"

/** 
 * RemoveTagsFromString:
 * 
 * @param string 
 * @param length 
 * 
 * @return 
 */
char* RemoveTagsFromString(char* string, int length){
    char* news = malloc(length+1);

    int s=0;
    int d=0;
    bool istag=true;
    
    /* remove starting spaces or tabs */
    while (string[s]==' ' || string[s]=='\t' || string[s]=='\n'){
        s++;
        printf("trovato");
    }

    /* scan all string. If you find a tag, ignore it until tag is closed */
    while(s<length){
        if (string[s]=='<')
            istag=true;

        if (string[s]=='>'){
            istag=false;
            s++;
        }

        /* character out <--->; store it on news buffer */
        if (!istag){
            news[d]=string[s];
            d++;
        }
        s++;
    }
    news[d+1]='\0';
    return news;
}


/**
 * PrintHTMLContents
 * Prints on screen just Ansa body of a news
 * @param html_url
 */
int PrintANSAContents(const char* html){
    char* start = strstr(html,ANSA_TAG);
    char* end = strstr(start,"</div>");
    int offset = end - start;

    char* news = RemoveTagsFromString(start,offset);
    PrintWithLenght(news,60);

    printf("%s\n",news);
    free(news);
    return 1;
}

int main(){
    FILE* html=fopen("./test_html.html","r");
    size_t size = 200*1024;
    char* data = malloc(size);
    fread(data,size,1,html);
    fclose(html);
    PrintANSAContents(data);
    free(data);

}
