
#include <ncurses.h>

#include "Screennews.h"

int main(int argc, char** argv){
    int v;
    int cursory=0;

    initscr();
    //start_color(); //initialize colors
    cbreak(); // key pressed doesn't require enter
    noecho(); // no echo on screen

    keypad(stdscr,1); //enables special keys

    char* rss_doc=AllocURL("http://www.ansa.it/sito/ansait_rss.xml");
    NewsList* newslist=ParseRSS(rss_doc);
    NewsObj* news;
    int first=0;
    int last=(newslist->total_news<COLS-1?newslist->total_news:COLS-1);

    while(v!='q'){
        for (int i=first; i<last; i++){
            news=newslist->allnews[i];
            mvprintw(i,0,"%s: %s",
                     news->title,
                     news->descr);
        }
        
        move(cursory,0);
        
        v=getch();
        
        if (v==KEY_UP){
            if (first-1>=0){
                first--;
                last--;
            }
            if (cursory-1>=0)
                cursory--;
        }
        
        else if (v==KEY_DOWN){
            if (last < newslist->total_news){
                first++;
                last++;
            }
            if (cursory+1<COLS-1)
                cursory++;
        }
        refresh();
    }
    endwin();
    free(rss_doc);
    free(newslist);
}
