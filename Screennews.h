
#pragma once

#include <stdio.h>
#include <string.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <curl/curl.h>

/* used by cURL library */
typedef struct{
  char *memory;
  size_t size;
} MemoryStruct;

/* A RSS news */
typedef struct{
    xmlChar* title;
    xmlChar* descr;
    xmlChar* link;
    xmlChar* pubdate;
}NewsObj;

/* Collection of NewsObj */
typedef struct{
    NewsObj* allnews[128];
    int total_news;
}NewsList;

/** 
 * NewsObj_New
 * Returns an empy NewsObj object allocated in RAM
 * 
 * @return 
 */
NewsObj* NewsObj_New();

/** 
 * AllocURL:
 * loads a remote resource as a char array. Allocate memory, must be freed
 * @param rss_url 
 * 
 * @return 
 */
char* AllocURL(const char* rss_url);

/** 
 * ParseRSS
 * Parses xml fetched as char*
 * @param xmldocument 
 */
NewsList* ParseRSS(char* xmldocument);
